package com.scrat.secretphoto.data.local.dao.gallery;

import android.support.annotation.Nullable;

import com.scrat.secretphoto.data.DataRepository;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.local.dao.lib.DbHelper;
import com.scrat.secretphoto.data.model.GalleryInfo;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static android.database.sqlite.SQLiteDatabase.CONFLICT_NONE;

public class GalleryDao {

    public long addGallery(GalleryEntry.Builder builder) {
        return DbHelper.getInstance().getDb()
                .insert(GalleryEntry.TABLE, CONFLICT_IGNORE, builder.build());
    }

    public void updateGallery(
            GalleryEntry.Builder builder,
            @Nullable String whereClause,
            @Nullable String... whereArgs) {
        DbHelper.getInstance().getDb()
                .update(GalleryEntry.TABLE, CONFLICT_NONE, builder.build(), whereClause, whereArgs);
    }

    public void increaseImgCount(long galleryId) {
        int totalImg = DataRepository.getInstance().getImgDao().getTotalCount(galleryId);
        if (galleryId == GalleryEntry.MAIN_ALBUM_GALLERY_ID) {
            Preferences.getInstance().setMainImgCount(totalImg);
            return;
        }
        if (galleryId == GalleryEntry.TRASH_GALLERY_ID) {
            Preferences.getInstance().setTrashImgCount(totalImg);
            return;
        }
        DataRepository.getInstance().getGalleryDao().updateGallery(
                GalleryEntry.Builder.newInstance().totalPhoto(totalImg),
                GalleryEntry.GALLERY_ID + "=?",
                String.valueOf(galleryId));
    }

    public void decreaseImgCount(long galleryId) {
        if (galleryId == GalleryEntry.MAIN_ALBUM_GALLERY_ID) {
            Preferences.getInstance().decreaseMainImgCount();
            return;
        }
        if (galleryId == GalleryEntry.TRASH_GALLERY_ID) {
            Preferences.getInstance().decreaseTrashImgCount();
            return;
        }
        String sql = "update " + GalleryEntry.TABLE +
                " set " + GalleryEntry.TOTAL_PHOTO + "=" + GalleryEntry.TOTAL_PHOTO + "-1" +
                " where " + GalleryEntry.GALLERY_ID + "=" + galleryId +
                " and " + GalleryEntry.TOTAL_PHOTO + ">0";
        DbHelper.getInstance().getDb().execute(sql);
    }

    public Flowable<List<GalleryInfo>> getGalleryList() {
        String sql = "select * from " + GalleryEntry.TABLE;
        return DbHelper.getInstance().getDb()
                .createQuery(GalleryEntry.TABLE, sql)
                .mapToList(GalleryEntry.MAPPER)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    public Flowable<GalleryInfo> getGallery(long galleryId) {
        String sql = "select * from " + GalleryEntry.TABLE + " where " + GalleryEntry.GALLERY_ID + "=? limit 1";
        return DbHelper.getInstance().getDb()
                .createQuery(GalleryEntry.TABLE, sql, galleryId)
                .mapToOne(GalleryEntry.MAPPER)
                .toFlowable(BackpressureStrategy.BUFFER);
    }
}
