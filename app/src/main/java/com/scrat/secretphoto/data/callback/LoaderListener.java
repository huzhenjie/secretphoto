package com.scrat.secretphoto.data.callback;

public interface LoaderListener<T> {
    void onLoadStart();

    void onLoadResult(T t);

    void onLoadCompleted();

    void onLoadError(String msg);
}
