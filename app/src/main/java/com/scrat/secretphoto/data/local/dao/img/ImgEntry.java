package com.scrat.secretphoto.data.local.dao.img;

import android.content.ContentValues;
import android.database.Cursor;

import com.scrat.secretphoto.data.local.dao.lib.BaseEntry;
import com.scrat.secretphoto.data.model.ImgInfo;

import io.reactivex.functions.Function;

public class ImgEntry extends BaseEntry {
    private ImgEntry() {
    }

    public static final String TABLE = "img";
    public static final String IMG_ID = "img_id";
    public static final String MEDIA_ID = "media_id";
    public static final String IMG_NAME = "img_name";
    public static final String GALLERY_ID = "gallery_id";
    public static final String ORIGIN_PATH = "origin_path";
    public static final String MD5 = "md5";
    public static final String SIZE = "size";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";

    public static final String CREATE_SQL =
            "CREATE TABLE " + TABLE + " (" +
                    IMG_ID + PRIMARY_KEY + "," +
                    MEDIA_ID + INTEGER + "," +
                    IMG_NAME + TEXT + "," +
                    GALLERY_ID + INTEGER + "," +
                    ORIGIN_PATH + TEXT + "," +
                    MD5 + TEXT + "," +
                    SIZE + INTEGER + "," +
                    CREATED_AT + INTEGER + "," +
                    UPDATED_AT + INTEGER + "," +
                    UNIQUE + "(" + GALLERY_ID + "," + MD5 + ")" +
                    " )";

    public static final Function<Cursor, ImgInfo> MAPPER = cursor -> new ImgInfo()
            .setImgId(getLong(cursor, IMG_ID))
            .setMediaId(getLong(cursor, MEDIA_ID))
            .setFileName(getString(cursor, IMG_NAME))
            .setGalleryId(getLong(cursor, GALLERY_ID))
            .setOriginPath(getString(cursor, ORIGIN_PATH))
            .setMd5(getString(cursor, MD5))
            .setFileSize(getLong(cursor, SIZE))
            .setCreatedAt(getLong(cursor, UPDATED_AT));

    public static final class Builder {
        private final ContentValues values = new ContentValues();

        public static ImgEntry.Builder newInstance() {
            return new ImgEntry.Builder();
        }

        public ContentValues build() {
            return values;
        }

        public ImgEntry.Builder mediaId(long mediaId) {
            values.put(MEDIA_ID, mediaId);
            return this;
        }

        public ImgEntry.Builder imgId(long imgId) {
            values.put(IMG_ID, imgId);
            return this;
        }

        public ImgEntry.Builder galleryId(long galleryId) {
            values.put(GALLERY_ID, galleryId);
            return this;
        }

        public ImgEntry.Builder md5(String md5) {
            values.put(MD5, md5);
            return this;
        }

        public ImgEntry.Builder originPath(String originPath) {
            values.put(ORIGIN_PATH, originPath);
            return this;
        }

        public ImgEntry.Builder fileName(String fileName) {
            values.put(IMG_NAME, fileName);
            return this;
        }

        public ImgEntry.Builder fileSize(long fileSize) {
            values.put(SIZE, fileSize);
            return this;
        }

        public ImgEntry.Builder createdAt(long createdAt) {
            values.put(CREATED_AT, createdAt);
            return this;
        }

        public ImgEntry.Builder updatedAt(long updatedAt) {
            values.put(UPDATED_AT, updatedAt);
            return this;
        }

    }
}
