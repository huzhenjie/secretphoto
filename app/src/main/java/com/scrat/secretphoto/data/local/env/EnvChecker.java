package com.scrat.secretphoto.data.local.env;

import android.content.Context;

import com.scrat.secretphoto.framework.util.Utils;

public class EnvChecker {
    // call by Application onCreate
    public static void refresh(Context applicationContext) {
        EnvPreferences preferences = EnvPreferences.getInstance();
        preferences.init(applicationContext);

        long currTs = System.currentTimeMillis();

        long times = preferences.getOpenAppTimes();
        preferences.setOpenAppTimes(times + 1L);

        long firstOpenAppTs = preferences.getFirstOpenAppTs();
        if (firstOpenAppTs == 0L) {
            preferences.setFirstOpenAppTs(currTs);
        }

        long lastOpenTs = preferences.getCurrOpenAppTs();
        preferences.setLastOpenAppTs(lastOpenTs);
        preferences.setCurrOpenAppTs(currTs);

        int currVerCode = Utils.getVersionCode(applicationContext);

        int firstOpenAppVerCode = preferences.getFirstOpenAppVerCode();
        if (firstOpenAppVerCode == 0) {
            preferences.setFirstOpenAppVerCode(currVerCode);
        }

        int lastVerCode = preferences.getCurrAppVerCode();
        if (lastVerCode != currVerCode) {
            preferences.setCurrAppVerCode(currVerCode);
            preferences.setLastAppVerCode(lastVerCode);
        }
    }

    public static boolean isNewVersionUser(Context applicationContext) {
        int appVerCode = Utils.getVersionCode(applicationContext);
        int firstOpenAppVerCode = EnvPreferences.getInstance().getFirstOpenAppVerCode();
        return firstOpenAppVerCode == appVerCode;
    }

}
