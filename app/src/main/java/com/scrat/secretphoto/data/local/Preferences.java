package com.scrat.secretphoto.data.local;

import android.content.Context;
import android.text.TextUtils;

import com.scrat.secretphoto.framework.common.BaseSharedPreferences;

public class Preferences extends BaseSharedPreferences {
    private static final String FILE_NAME = "conf";

    private static class SingletonHolder {
        private static Preferences instance = new Preferences();
    }

    public static Preferences getInstance() {
        return SingletonHolder.instance;
    }

    public void init(Context applicationContext) {
        init(applicationContext, FILE_NAME);
    }

    private static final String TOTAL_MAIN_IMG_COUNT = "total_img_count";

    public void increaseMainImgCount() {
        int total = getInt(TOTAL_MAIN_IMG_COUNT, 0);
        setInt(TOTAL_MAIN_IMG_COUNT, total + 1);
    }

    public void setMainImgCount(int count) {
        setInt(TOTAL_MAIN_IMG_COUNT, count);
    }

    public void decreaseMainImgCount() {
        int total = getInt(TOTAL_MAIN_IMG_COUNT, 0);
        if (total == 0) {
            return;
        }
        setInt(TOTAL_MAIN_IMG_COUNT, total - 1);
    }

    public int getTotalMainImgCOunt() {
        return getInt(TOTAL_MAIN_IMG_COUNT, 0);
    }

    private static final String TOTAL_TRASH_IMG_COUNT = "total_trash_img_count";

    public void increaseTrashImgCount() {
        int total = getInt(TOTAL_TRASH_IMG_COUNT, 0);
        setInt(TOTAL_TRASH_IMG_COUNT, total + 1);
    }

    public void decreaseTrashImgCount() {
        int total = getInt(TOTAL_TRASH_IMG_COUNT, 0);
        if (total == 0) {
            return;
        }
        setInt(TOTAL_TRASH_IMG_COUNT, total - 1);
    }

    public void setTrashImgCount(int count) {
        setInt(TOTAL_TRASH_IMG_COUNT, count);
    }

    public int getTotalTrashImgCount() {
        return getInt(TOTAL_TRASH_IMG_COUNT, 0);
    }

    private static final String HAS_SHOWN_GUIDE = "has_shown_guide";

    public boolean hasShownGuide() {
        return getBoolean(HAS_SHOWN_GUIDE, false);
    }

    public void setShowGuide(boolean isShow) {
        setBoolean(HAS_SHOWN_GUIDE, isShow);
    }

    private static final String PIN_CODE_PASSWD = "pin_code_passwd";

    public String getPinCodePasswd() {
        // TODO 加密
        return getString(PIN_CODE_PASSWD, "");
    }

    public void setPinCodePasswd(String passwd) {
        // TODO 解密
        setString(PIN_CODE_PASSWD, passwd);
    }

    public boolean hasSetPasswd() {
        String passwd = getPinCodePasswd();
        return !TextUtils.isEmpty(passwd);
    }

}
