package com.scrat.secretphoto.data.local.dao.gallery;

import android.content.ContentValues;
import android.database.Cursor;

import com.scrat.secretphoto.data.local.dao.lib.BaseEntry;
import com.scrat.secretphoto.data.model.GalleryInfo;

import io.reactivex.functions.Function;

public class GalleryEntry extends BaseEntry {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private GalleryEntry() {
    }

    public static final String TABLE = "gallery";
    public static final String GALLERY_ID = "gallery_id";
    public static final String GALLERY_NAME = "gallery_name";
    public static final String COVER = "cover";
    public static final String TOTAL_PHOTO = "total_photo";
    public static final String TOTAL_VIDEO = "total_video";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";

    public static final long TRASH_GALLERY_ID = -1L;
    public static final long MAIN_ALBUM_GALLERY_ID = 0L;

    public static final String CREATE_SQL =
            "CREATE TABLE " + TABLE + " (" +
                    GALLERY_ID + PRIMARY_KEY + "," +
                    GALLERY_NAME + TEXT + "," +
                    COVER + TEXT + "," +
                    TOTAL_PHOTO + INTEGER + "," +
                    TOTAL_VIDEO + INTEGER + "," +
                    CREATED_AT + INTEGER + "," +
                    UPDATED_AT + INTEGER +
                    " )";

    public static final Function<Cursor, GalleryInfo> MAPPER = cursor -> new GalleryInfo()
            .setGalleryId(getLong(cursor, GALLERY_ID))
            .setCover(getString(cursor, COVER))
            .setTitle(getString(cursor, GALLERY_NAME))
            .setTotalPhoto(getInt(cursor, TOTAL_PHOTO))
            .setTotalVideo(getInt(cursor, TOTAL_VIDEO));


    public static final class Builder {
        private final ContentValues values = new ContentValues();

        public static Builder newInstance() {
            return new Builder();
        }

        public GalleryEntry.Builder galleryId(long galleryId) {
            values.put(GALLERY_ID, galleryId);
            return this;
        }

        public GalleryEntry.Builder cover(String cover) {
            values.put(COVER, cover);
            return this;
        }

        public GalleryEntry.Builder galleryName(String galleryName) {
            values.put(GALLERY_NAME, galleryName);
            return this;
        }

        public GalleryEntry.Builder totalPhoto(int totalPhoto) {
            values.put(TOTAL_PHOTO, totalPhoto);
            return this;
        }

        public GalleryEntry.Builder totalVideo(int totalVideo) {
            values.put(TOTAL_PHOTO, totalVideo);
            return this;
        }

        public GalleryEntry.Builder createdAt(long createdAt) {
            values.put(CREATED_AT, createdAt);
            return this;
        }

        public GalleryEntry.Builder updatedAt(long updatedAt) {
            values.put(UPDATED_AT, updatedAt);
            return this;
        }

        public ContentValues build() {
            return values;
        }
    }
}
