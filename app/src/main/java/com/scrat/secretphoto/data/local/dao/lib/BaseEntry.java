package com.scrat.secretphoto.data.local.dao.lib;

import android.database.Cursor;

import io.reactivex.functions.Function;

public class BaseEntry {
    public static final int BOOLEAN_FALSE = 0;
    public static final int BOOLEAN_TRUE = 1;
    protected static final String PRIMARY_KEY = " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT";
    protected static final String TEXT = " TEXT NOT NULL DEFAULT ''";
    protected static final String INTEGER = " INTEGER NOT NULL DEFAULT 0";
    protected static final String UNIQUE = " UNIQUE";

    public static final Function<Cursor, Long> LONG_MAPPER = cursor -> getLong(cursor, 0);

    public static final Function<Cursor, Boolean> EXIST_MAPPER = cursor -> getLong(cursor, 0) > 0;

    public static String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    public static boolean getBoolean(Cursor cursor, String columnName) {
        return getInt(cursor, columnName) == BOOLEAN_TRUE;
    }

    public static long getLong(Cursor cursor, int colIndex) {
        return cursor.getLong(colIndex);
    }

    public static long getLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
    }

    public static int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }
}
