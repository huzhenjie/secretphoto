package com.scrat.secretphoto.data.model;

import android.databinding.BaseObservable;
import android.text.TextUtils;

import com.scrat.secretphoto.data.FileManager;

import java.io.File;
import java.io.Serializable;

public class ImgInfo extends BaseObservable implements Serializable {
    private long mediaId;// local
    private long imgId;
    private long galleryId;
    private String md5;
    private String originPath;
    private String fileName;
    private long fileSize;
    private long createdAt;

    private boolean checked;

    public long getMediaId() {
        return mediaId;
    }

    public ImgInfo setMediaId(long mediaId) {
        this.mediaId = mediaId;
        return this;
    }

    public long getGalleryId() {
        return galleryId;
    }

    public ImgInfo setGalleryId(long galleryId) {
        this.galleryId = galleryId;
        return this;
    }

    public long getImgId() {
        return imgId;
    }

    public ImgInfo setImgId(long imgId) {
        this.imgId = imgId;
        return this;
    }

    public String getMd5() {
        return md5;
    }

    public ImgInfo setMd5(String md5) {
        this.md5 = md5;
        return this;
    }

    public String getAppPath() {
        return FileManager.getAbsSecretImgFolder().getPath() + File.separator + getMd5();
    }

    public String getOriginPath() {
        return originPath;
    }

    public ImgInfo setOriginPath(String originPath) {
        this.originPath = originPath;
        return this;
    }

    public String getFileName() {
        if (!TextUtils.isEmpty(fileName)) {
            return fileName;
        }
        if (TextUtils.isEmpty(originPath)) {
            return "";
        }
        return originPath.substring(originPath.lastIndexOf("/") + 1, originPath.length());
    }

    public ImgInfo setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public long getFileSize() {
        return fileSize;
    }

    public ImgInfo setFileSize(long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public ImgInfo setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public boolean isChecked() {
        return checked;
    }

    public ImgInfo setChecked(boolean checked) {
        this.checked = checked;
        return this;
    }

    @Override
    public String toString() {
        return "ImgInfo{" +
                "mediaId=" + mediaId +
                ", imgId=" + imgId +
                ", galleryId=" + galleryId +
                ", md5='" + md5 + '\'' +
                ", originPath='" + originPath + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", createdAt=" + createdAt +
                ", checked=" + checked +
                '}';
    }
}
