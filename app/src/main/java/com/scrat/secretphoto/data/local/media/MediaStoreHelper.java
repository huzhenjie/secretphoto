package com.scrat.secretphoto.data.local.media;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.scrat.secretphoto.data.model.ImgInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class MediaStoreHelper {
    private static void close(Cursor c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<ImgInfo> getMediaStoreImgList(Context ctx) {
        List<ImgInfo> imgList = new ArrayList<>();
        ContentResolver resolver = ctx.getContentResolver();
        String[] projection = new String[]{
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.MediaColumns.SIZE
        };
        String selection = MediaStore.MediaColumns.SIZE + ">?";
        String[] selectionArgs = new String[]{"0"};
        String sortOrder = MediaStore.Images.Media._ID + " desc";
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor c = resolver.query(uri, projection, selection, selectionArgs, sortOrder);
        if (c == null) {
            return imgList;
        }
        while (c.moveToNext()) {
            imgList.add(new ImgInfo()
                    .setMediaId(c.getLong(c.getColumnIndex(MediaStore.Images.Media._ID)))
                    .setOriginPath(c.getString(c.getColumnIndex(MediaStore.Images.Media.DATA)))
                    .setFileSize(c.getLong(c.getColumnIndex(MediaStore.MediaColumns.SIZE)))
            );
        }
        close(c);
        return imgList;
    }

    public static Observable<List<ImgInfo>> getImgList(Context ctx) {
        return Observable.create(emitter -> {
            List<ImgInfo> list = getMediaStoreImgList(ctx);
            emitter.onNext(list);
        });

//        Consumer<List<ImgInfo>> consumer = list -> {
//            for (ImgInfo info : list) {
//                L.d("%s", info);
//            }
//        };
//
//        Disposable disposable = observable.subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(consumer);
    }

    public static Observable<List<ImgInfo>> removeImgList(Context ctx, List<ImgInfo> list) {
        return Observable.create(emitter -> {
            ContentResolver contentResolver = ctx.getContentResolver();
            List<ImgInfo> fail = new ArrayList<>();
            for (ImgInfo info : list) {
                Uri deleteUri = ContentUris.withAppendedId(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, info.getMediaId());
                contentResolver.delete(deleteUri, null, null);
                File file = new File(info.getOriginPath());
                if (file.exists()) {
                    file.delete();
                }
                ctx.sendBroadcast(
                        new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                if (file.exists()) {
                    fail.add(info);
                }
            }
            emitter.onNext(fail);
        });
    }
}
