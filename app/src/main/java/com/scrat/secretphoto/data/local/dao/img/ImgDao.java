package com.scrat.secretphoto.data.local.dao.img;

import android.database.Cursor;
import android.support.annotation.Nullable;

import com.scrat.secretphoto.data.FileManager;
import com.scrat.secretphoto.data.local.dao.lib.BaseEntry;
import com.scrat.secretphoto.data.local.dao.lib.DbHelper;
import com.scrat.secretphoto.data.model.ImgInfo;

import java.io.File;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static android.database.sqlite.SQLiteDatabase.CONFLICT_NONE;

public class ImgDao {
    public Flowable<List<ImgInfo>> getImgList(long galleryId) {
        String sql = "select * from " + ImgEntry.TABLE + " where " + ImgEntry.GALLERY_ID + "=?";
        return DbHelper.getInstance().getDb()
                .createQuery(ImgEntry.TABLE, sql, galleryId)
                .mapToList(ImgEntry.MAPPER)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    public long addImg(ImgEntry.Builder builder) {
        return DbHelper.getInstance().getDb()
                .insert(ImgEntry.TABLE, CONFLICT_IGNORE, builder.build());
    }

    public Flowable<Long> count(long galleryId) {
        String sql = "select count(1) from " + ImgEntry.TABLE + " where " + ImgEntry.GALLERY_ID + "=?";
        return DbHelper.getInstance().getDb()
                .createQuery(ImgEntry.TABLE, sql, galleryId)
                .mapToOne(BaseEntry.LONG_MAPPER)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    public int getTotalCount(long galleryId) {
        String sql = "select count(1) from " + ImgEntry.TABLE + " where " + ImgEntry.GALLERY_ID + "=" + galleryId;
        Cursor c = DbHelper.getInstance().getDb().getReadableDatabase().query(sql);
        c.moveToFirst();
        return c.getInt(0);
    }

    public String getCover(long galleryId) {
        String sql = "select " + ImgEntry.MD5 + " from " + ImgEntry.TABLE + " where " + ImgEntry.GALLERY_ID + "=" + galleryId + " order by " + ImgEntry.CREATED_AT + " desc limit 1";
        Cursor c = DbHelper.getInstance().getDb().getReadableDatabase().query(sql);
        c.moveToFirst();
        String cover = c.getString(0);
        if (cover == null) {
            return "";
        }
        return FileManager.getAbsSecretImgFolder().getPath() + File.separator + cover;
    }

    public Flowable<Boolean> isExist(long galleryId, long imgId) {
        String sql = "select count(1) from " + ImgEntry.TABLE + " where " + ImgEntry.GALLERY_ID + "=? and " + ImgEntry.IMG_ID + "=?";
        return DbHelper.getInstance().getDb()
                .createQuery(ImgEntry.TABLE, sql, galleryId, imgId)
                .mapToOne(BaseEntry.EXIST_MAPPER)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    public boolean deleteImg(long galleryId, String md5) {
        int executeRow = DbHelper.getInstance().getDb().delete(
                ImgEntry.TABLE,
                ImgEntry.GALLERY_ID + "=? and " + ImgEntry.MD5 + "=?",
                String.valueOf(galleryId), md5);
        return executeRow > 0;
    }

    public void updateImg(
            ImgEntry.Builder builder,
            @Nullable String whereClause,
            @Nullable String... whereArgs) {
        DbHelper.getInstance().getDb()
                .update(ImgEntry.TABLE, CONFLICT_NONE, builder.build(), whereClause, whereArgs);
    }
}
