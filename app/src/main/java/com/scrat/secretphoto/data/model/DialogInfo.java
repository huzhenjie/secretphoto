package com.scrat.secretphoto.data.model;

public class DialogInfo {
    private String title;
    private String content;
    private String confirmText;
    private String cancelText;

    public String getTitle() {
        return title;
    }

    public DialogInfo setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public DialogInfo setContent(String content) {
        this.content = content;
        return this;
    }

    public String getConfirmText() {
        return confirmText;
    }

    public DialogInfo setConfirmText(String confirmText) {
        this.confirmText = confirmText;
        return this;
    }

    public String getCancelText() {
        return cancelText;
    }

    public DialogInfo setCancelText(String cancelText) {
        this.cancelText = cancelText;
        return this;
    }
}
