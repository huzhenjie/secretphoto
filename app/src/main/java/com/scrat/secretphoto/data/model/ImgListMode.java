package com.scrat.secretphoto.data.model;

import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;

import java.io.Serializable;

public class ImgListMode implements Serializable {
    private int mode;
    private int totalChecked;
    private int total;
    private String galleryName;
    private long galleryId;

    public static ImgListMode newSysMode() {
        return new ImgListMode().setMode(1);
    }

    public static ImgListMode newLocalMode() {
        return new ImgListMode();
    }

    public ImgListMode setMode(int mode) {
        this.mode = mode;
        return this;
    }

    public boolean isLocalList() {
        return mode == 0;
    }

    public boolean canShowFab() {
        return isLocalList() && !isTrash();
    }

    public boolean isImportImg() {
        return mode == 1;
    }

    public int getMode() {
        return mode;
    }

    public int getTotal() {
        return total;
    }

    public ImgListMode setTotal(int total) {
        this.total = total;
        return this;
    }

    public int getTotalChecked() {
        return totalChecked;
    }

    public ImgListMode setTotalChecked(int totalChecked) {
        this.totalChecked = totalChecked;
        return this;
    }

    public String getGalleryName() {
        return galleryName;
    }

    public ImgListMode setGalleryName(String galleryName) {
        this.galleryName = galleryName;
        return this;
    }

    public long getGalleryId() {
        return galleryId;
    }

    public ImgListMode setGalleryId(long galleryId) {
        this.galleryId = galleryId;
        return this;
    }

    public boolean isTrash() {
        return galleryId == GalleryEntry.TRASH_GALLERY_ID;
    }

    public boolean canShowDefaultCover() {
        return isLocalList() && total == 0;
    }
}
