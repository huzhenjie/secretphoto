package com.scrat.secretphoto.data.model;

import java.io.Serializable;

public class PinCodeViewModel implements Serializable {
    private String selectedCode;
    private OnCodeListener listener;
    private int state;

    public PinCodeViewModel(OnCodeListener listener) {
        this.listener = listener;
        state = 0;
        selectedCode = "";
    }

    public String getSelectedCode() {
        return selectedCode;
    }

    public PinCodeViewModel appendSelectedCode(String code) {
        if (selectedCode != null && selectedCode.length() >= 4) {
            return this;
        }
        selectedCode += code;
        if (selectedCode.length() >= 4) {
            listener.verify(selectedCode);
        }
        return this;
    }

    public PinCodeViewModel clear() {
        selectedCode = "";
        return this;
    }

    public boolean isShowSelected1() {
        return selectedCode != null && selectedCode.length() >= 1;
    }

    public boolean isShowSelected2() {
        return selectedCode != null && selectedCode.length() >= 2;
    }

    public boolean isShowSelected3() {
        return selectedCode != null && selectedCode.length() >= 3;
    }

    public boolean isShowSelected4() {
        return selectedCode != null && selectedCode.length() >= 4;
    }

    public interface OnCodeListener {
        void verify(String code);
    }

    public void setPasswordStep1() {
        state = 1;
    }

    public void setPasswordStep2() {
        state = 2;
    }

    public boolean isSetPasswordStep1() {
        return state == 1;
    }

    public boolean isSetPasswordStep2() {
        return state == 2;
    }

    public boolean isCustomVerify() {
        return state == 0;
    }

}
