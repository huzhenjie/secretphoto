package com.scrat.secretphoto.data.model;

import android.content.Context;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;

import java.io.Serializable;

public class GalleryInfo implements Serializable {

    private long galleryId;
    private String cover;
    private String title;
    private int totalPhoto;
    private int totalVideo;

    private static GalleryInfo TRASH;
    private static GalleryInfo MAIN_ALBUM;

    public static GalleryInfo getTrash(Context applicationContext) {
        if (TRASH == null) {
            TRASH = new GalleryInfo()
                    .setGalleryId(GalleryEntry.TRASH_GALLERY_ID)
                    .setTitle(applicationContext.getString(R.string.trash));
        }
        return TRASH;
    }

    public static GalleryInfo getMainAlbum(Context applicationContext) {
        if (MAIN_ALBUM == null) {
            MAIN_ALBUM = new GalleryInfo()
                    .setGalleryId(GalleryEntry.MAIN_ALBUM_GALLERY_ID)
                    .setTitle(applicationContext.getString(R.string.main_album));
        }
        return MAIN_ALBUM;
    }

    public long getGalleryId() {
        return galleryId;
    }

    public GalleryInfo setGalleryId(long galleryId) {
        this.galleryId = galleryId;
        return this;
    }

    public String getCover() {
        return cover;
    }

    public GalleryInfo setCover(String cover) {
        this.cover = cover;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public GalleryInfo setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getTotalPhoto() {
        if (galleryId == GalleryEntry.MAIN_ALBUM_GALLERY_ID) {
            return Preferences.getInstance().getTotalMainImgCOunt();
        }
        if (galleryId == GalleryEntry.TRASH_GALLERY_ID) {
            return Preferences.getInstance().getTotalTrashImgCount();
        }
        return totalPhoto;
    }

    public GalleryInfo setTotalPhoto(int totalPhoto) {
        this.totalPhoto = totalPhoto;
        return this;
    }

    public int getTotalVideo() {
        return totalVideo;
    }

    public GalleryInfo setTotalVideo(int totalVideo) {
        this.totalVideo = totalVideo;
        return this;
    }

}
