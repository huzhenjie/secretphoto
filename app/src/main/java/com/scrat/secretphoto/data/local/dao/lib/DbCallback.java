package com.scrat.secretphoto.data.local.dao.lib;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.content.Context;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;
import com.scrat.secretphoto.data.local.dao.img.ImgEntry;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static android.database.sqlite.SQLiteDatabase.CONFLICT_NONE;

public final class DbCallback extends SupportSQLiteOpenHelper.Callback {
    private static final int VERSION = 1;
    private Context context;

    DbCallback(Context applicationContext) {
        super(VERSION);
        context = applicationContext;
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {
        db.execSQL(ImgEntry.CREATE_SQL);
        db.execSQL(GalleryEntry.CREATE_SQL);
//        db.insert(GalleryEntry.TABLE, CONFLICT_IGNORE, GalleryEntry.Builder.newInstance()
//                .createdAt(System.currentTimeMillis())
//                .galleryName(context.getString(R.string.main_album))
//                .build());
    }

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
