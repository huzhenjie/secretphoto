package com.scrat.secretphoto.data;

import android.os.Environment;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

public class FileManager {
    private static File getAbsSecretFolder() {
        File folder = new File(
                Environment.getExternalStorageDirectory(), ".secret.photo");
        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder;
    }

    public static File getAbsSecretImgFolder() {
        File folder = getAbsSecretFolder();
        File imgFolder = new File(folder, "img");
        if (!imgFolder.exists()) {
            imgFolder.mkdir();
        }

        return imgFolder;
    }

    public static String getAbsDbFilePath() {
        File folder = getAbsSecretFolder();
        return folder.getAbsolutePath() + File.separator + "SecretPhoto.db";
    }

    public static String md5(File file) {
        if (file == null || !file.isFile() || !file.exists()) {
            return "";
        }
        FileInputStream in = null;
        byte buffer[] = new byte[8192];
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            int len;
            while ((len = in.read(buffer)) != -1) {
                md5.update(buffer, 0, len);
            }
            byte[] bytes = md5.digest();

            StringBuilder result = new StringBuilder();
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result.append(temp);
            }
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            close(in);
        }
    }

    public static boolean copy(File src, File dst) {
        FileInputStream inStream = null;
        FileOutputStream outStream = null;
        try {
            inStream = new FileInputStream(src);
            outStream = new FileOutputStream(dst);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            return true;
        } catch (Throwable e) {
            return false;
        } finally {
            close(inStream);
            close(outStream);
        }
    }

    public static void close(Closeable c) {
        if (c == null) {
            return;
        }

        try {
            c.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
