package com.scrat.secretphoto.data.local.dao.lib;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;

import com.scrat.secretphoto.BuildConfig;
import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import io.reactivex.schedulers.Schedulers;

public class DbHelper {
    private static class SingletonHolder {
        private static DbHelper instance = new DbHelper();
    }

    public static DbHelper getInstance() {
        return DbHelper.SingletonHolder.instance;
    }

    private BriteDatabase db;
    private static final String DB_NAME = "SecretPhoto.db";

    private DbHelper() {
        // Singleton only
    }

    public void init(Context applicationContext) {
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        SupportSQLiteOpenHelper.Configuration configuration = SupportSQLiteOpenHelper
                .Configuration.builder(applicationContext)
                .name(DB_NAME)
                .callback(new DbCallback(applicationContext))
                .build();
        SupportSQLiteOpenHelper.Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper helper = factory.create(configuration);
        db = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
        if (BuildConfig.DEBUG) {
            db.setLoggingEnabled(true);
        }
    }

    public BriteDatabase getDb() {
        return db;
    }
}
