package com.scrat.secretphoto.data;

import com.scrat.secretphoto.data.local.dao.gallery.GalleryDao;
import com.scrat.secretphoto.data.local.dao.img.ImgDao;

public class DataRepository {
    private static class SingletonHolder {
        private static DataRepository instance = new DataRepository();
    }

    public static DataRepository getInstance() {
        return DataRepository.SingletonHolder.instance;
    }

    private GalleryDao galleryDao;
    private ImgDao imgDao;

    private DataRepository() {
        // Singleton only
        galleryDao = new GalleryDao();
        imgDao = new ImgDao();
    }

    public GalleryDao getGalleryDao() {
        return galleryDao;
    }

    public ImgDao getImgDao() {
        return imgDao;
    }
}
