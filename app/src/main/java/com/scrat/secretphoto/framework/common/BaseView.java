package com.scrat.secretphoto.framework.common;

import android.content.Context;

public interface BaseView<T> {

    Context getApplicationContext();

    void setPresenter(T presenter);

}
