package com.scrat.secretphoto.framework.glide;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.scrat.secretphoto.R;

public class GlideHelper {
    @BindingAdapter({"coverImageUrl"})
    public static void loadCoverImage(ImageView imageView, String url) {
        Context ctx = imageView.getContext();
        RequestOptions options = RequestOptions
                .centerCropTransform()
                .transform(new GlideRoundTransform());
        GlideApp.with(ctx)
                .load(url)
                .apply(options)
                .placeholder(R.drawable.bg_accent_10)
                .error(R.drawable.bg_accent_10)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @BindingAdapter({"squareImageUrl"})
    public static void loadSquareImage(ImageView imageView, String url) {
        Context ctx = imageView.getContext();
        GlideApp.with(ctx)
                .load(url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @BindingAdapter({"fullImageUrl"})
    public static void loadFullImage(ImageView imageView, String url) {
        Context ctx = imageView.getContext();
        GlideApp.with(ctx)
                .load(url)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }
}
