package com.scrat.secretphoto.framework.common;

public class SimpleRecyclerViewAdapter<T> extends BaseRecyclerViewAdapter<T> {

    private int layoutId;

    public SimpleRecyclerViewAdapter(int layoutId) {
        this.layoutId = layoutId;
    }

    @Override
    protected int getLayoutId(int viewType) {
        return layoutId;
    }
}
