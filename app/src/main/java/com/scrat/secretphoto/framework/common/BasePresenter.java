package com.scrat.secretphoto.framework.common;

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
