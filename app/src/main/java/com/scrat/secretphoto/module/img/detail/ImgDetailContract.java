package com.scrat.secretphoto.module.img.detail;

import com.scrat.secretphoto.framework.common.BasePresenter;
import com.scrat.secretphoto.framework.common.BaseView;

public interface ImgDetailContract {
    interface View extends BaseView<Presenter> {
        void showDeleteImgSuccess();
    }

    interface Presenter extends BasePresenter {
        void deleteImg();
    }
}
