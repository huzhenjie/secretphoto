package com.scrat.secretphoto.module.settting.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.databinding.ActivityGallerySettingBinding;
import com.scrat.secretphoto.framework.common.BaseActivity;

public class GallerySettingActivity extends BaseActivity {
    private ActivityGallerySettingBinding binding;
    private static final String DATA = "data";

    public static void show(Activity activity, int requestCode, GalleryInfo info) {
        Intent i = new Intent(activity, GallerySettingActivity.class);
        i.putExtra(DATA, info);
        activity.startActivityForResult(i, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery_setting);
        GalleryInfo info = (GalleryInfo) getIntent().getSerializableExtra(DATA);
        binding.setItem(info);
    }

    public void showModifyAlbumName(View view) {

    }
}
