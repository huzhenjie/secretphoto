package com.scrat.secretphoto.module.img.detail;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.model.DialogInfo;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.data.model.ImgPagerViewModel;
import com.scrat.secretphoto.databinding.ActivityImgDetailBinding;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.framework.util.L;
import com.scrat.secretphoto.widget.DialogPopupWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImgPagerActivity extends BaseActivity implements ImgPagerContract.View {
    private static final String INDEX = "index";
    private static final String DATA = "data";

    public static void show(Activity activity, int requestCode, GalleryInfo info, int index) {
        Intent i = new Intent(activity, ImgPagerActivity.class);
        i.putExtra(INDEX, index);
        i.putExtra(DATA, info);
        activity.startActivityForResult(i, requestCode);
    }

    private ImgListAdapter adapter;
    private ImgPagerContract.Presenter presenter;
    private ViewPager pager;
    private int index;
    private ActivityImgDetailBinding binding;
    private ImgPagerViewModel viewModel;
    private DialogPopupWindow dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_img_detail);
        viewModel = new ImgPagerViewModel();
        GalleryInfo info = (GalleryInfo) getIntent().getSerializableExtra(DATA);
        index = getIntent().getIntExtra(INDEX, 0);
        adapter = new ImgListAdapter(getSupportFragmentManager());
        pager = findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ImgInfo imgInfo = adapter.getImgInfo(position);
                refreshUi(imgInfo);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        new ImgPagerPresenter(this, info);
        presenter.loadImgList();
        initDialog();
        binding.delBtn.setOnClickListener(this::deleteImg);
    }

    private void initDialog() {
        dialog = new DialogPopupWindow(
                this,
                new DialogInfo()
                        .setTitle(getString(R.string.delete_photo))
                        .setContent(getString(R.string.delete_photo_content))
                        .setCancelText(getString(R.string.no))
                        .setConfirmText(getString(R.string.yes))
        );
        registerPopupWindow(dialog);
    }

    public void deleteImg(View view) {
        L.d("count=%s", adapter.getCount());
        dialog.showAsDropDown(binding.topBar);
    }

    public void cancelDialog(View view) {
        dialog.dismiss();
    }

    public void confirmDialog(View view) {
        dialog.dismiss();
        ImgInfo info = adapter.getImgInfo(pager.getCurrentItem());
        presenter.deleteImg(info);
    }

    public void share(View view) {

    }

    public void export(View view) {

    }

    private void refreshUi(ImgInfo imgInfo) {
        if (imgInfo == null) {
            return;
        }
        viewModel.setTitle(imgInfo.getFileName());
        binding.setViewModel(viewModel);
        binding.executePendingBindings();
    }

    @Override
    public void showImg(List<ImgInfo> list) {
        adapter.replaceData(list);
        if (index >= list.size()) {
            return;
        }
        pager.setCurrentItem(index);
        ImgInfo imgInfo = list.get(index);
        refreshUi(imgInfo);
        L.d(list.toString());
        L.d("coutn=%s", adapter.getCount());
    }

    @Override
    public void showDeleteImgSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void setPresenter(ImgPagerContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private static class ImgListAdapter extends FragmentPagerAdapter {

        private List<ImgInfo> list;
        private Map<Integer, Fragment> fragmentMap;

        private ImgListAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            list = new ArrayList<>();
            fragmentMap = new HashMap<>();
        }

        private ImgInfo getImgInfo(int position) {
            return list.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            if (position >= list.size()) {
                return null;
            }
            ImgInfo info = list.get(position);
            Fragment fragment = fragmentMap.get(position);
            if (fragment == null) {
                fragment = ImgPagerFragment.newInstance(info);
                fragmentMap.put(position, fragment);
            }
            return fragment;
        }

        private void replaceData(List<ImgInfo> list) {
            this.list.clear();
            if (list != null && !list.isEmpty()) {
                this.list.addAll(list);
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }
}
