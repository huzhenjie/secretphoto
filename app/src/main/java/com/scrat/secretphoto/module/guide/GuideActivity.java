package com.scrat.secretphoto.module.guide;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.framework.util.PermissionUtil;
import com.scrat.secretphoto.module.gallery.GalleryActivity;

public class GuideActivity extends BaseActivity {
    public static void show(Context ctx) {
        Intent i = new Intent(ctx, GuideActivity.class);
        ctx.startActivity(i);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allPermissionGranted = true;
        for (int grantRes : grantResults) {
            if (grantRes == -1) {
                allPermissionGranted = false;
            }
        }
        if (!allPermissionGranted) {
            return;
        }

        showNext();
    }

    public void start(View view) {
        boolean permissionGranted = PermissionUtil.checkPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionGranted) {
            showNext();
        }
    }

    private void showNext() {
        Preferences.getInstance().setShowGuide(true);
        GalleryActivity.showFromGuide(this);
        finish();
    }
}
