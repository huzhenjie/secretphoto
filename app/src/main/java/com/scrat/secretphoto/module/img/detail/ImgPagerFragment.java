package com.scrat.secretphoto.module.img.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.framework.common.BaseFragment;
import com.scrat.secretphoto.framework.glide.GlideApp;

public class ImgPagerFragment extends BaseFragment {
    private static final String DATA = "data";

    public static ImgPagerFragment newInstance(ImgInfo info) {

        Bundle args = new Bundle();
        args.putSerializable(DATA, info);
        ImgPagerFragment fragment = new ImgPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_img_item, container, false);
        ImgInfo info = (ImgInfo) getArguments().getSerializable(DATA);
        ImageView imageView = root.findViewById(R.id.img);
        GlideApp.with(this).load(info.getAppPath()).into(imageView);
        return root;
    }
}
