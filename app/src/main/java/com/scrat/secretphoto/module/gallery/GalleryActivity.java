package com.scrat.secretphoto.module.gallery;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.model.DialogInfo;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.data.model.ImgListMode;
import com.scrat.secretphoto.databinding.ActivityGalleryBinding;
import com.scrat.secretphoto.databinding.ListItemGalleryBinding;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.framework.common.BaseRecyclerViewAdapter;
import com.scrat.secretphoto.framework.common.BaseRecyclerViewHolder;
import com.scrat.secretphoto.framework.util.L;
import com.scrat.secretphoto.framework.util.PermissionUtil;
import com.scrat.secretphoto.framework.viewanimator.ViewAnimator;
import com.scrat.secretphoto.module.img.list.SysImgListActivity;
import com.scrat.secretphoto.module.lock.pin.PinLockActivity;
import com.scrat.secretphoto.module.settting.main.MainSettingActivity;
import com.scrat.secretphoto.widget.DialogPopupWindow;

import java.util.List;

public class GalleryActivity extends BaseActivity implements GalleryContract.View {
    private static final int REQUEST_CODE_IMPORT_IMG = 1;
    private static final int REQUEST_CODE_VIEW_IMG_LIST = 2;
    private static final String STATE = "state";
    private static final int STATE_DEFAULT = 0;
    private static final int STATE_GUIDE = 1;

    public static void show(Context ctx) {
        Intent i = new Intent(ctx, GalleryActivity.class);
        ctx.startActivity(i);
    }

    public static void showFromGuide(Context ctx) {
        Intent i = new Intent(ctx, GalleryActivity.class);
        i.putExtra(STATE, STATE_GUIDE);
        ctx.startActivity(i);
    }

    private ActivityGalleryBinding binding;
    private GalleryAdapter adapter;
    private GalleryContract.Presenter presenter;
    private AlertDialog addDlg;
    private EditText contentEt;
    private DialogPopupWindow passwdDlg;
    private boolean fabOpen;
    private int state;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);
        initSwipeRefreshLayout(binding.srf);
        initListView();
        initDialog();

        new GalleryPresenter(this);
        PermissionUtil.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        loadData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        loadData();
    }

    private void loadData() {
        state = getIntent().getIntExtra(STATE, STATE_DEFAULT);
        switch (state) {
            case STATE_GUIDE:
                showImgList(GalleryInfo.getMainAlbum(this), ImgListMode.newSysMode());
                break;
        }
        presenter.loadGalleryList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_VIEW_IMG_LIST:
                if (state == STATE_GUIDE) {
                    binding.list.postDelayed(() -> passwdDlg.showAsDropDown(binding.topBarTitle), 500L);
                }
                presenter.loadGalleryList();
                break;
        }

    }

    @Override
    protected void initSwipeRefreshLayout(SwipeRefreshLayout srl) {
        super.initSwipeRefreshLayout(srl);
        srl.setOnRefreshListener(() -> presenter.loadGalleryList());
    }

    @Override
    protected void onDestroy() {
        if (addDlg.isShowing()) {
            addDlg.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (fabOpen) {
            closeMenu();
            return;
        }
        super.onBackPressed();
    }

    private void initDialog() {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_window_add_album, null);
        contentEt = v.findViewById(R.id.album_name);
        v.findViewById(R.id.cancel_btn).setOnClickListener(v2 -> {
            addDlg.dismiss();
        });
        v.findViewById(R.id.ok_btn).setOnClickListener(v1 -> {
            presenter.addGallery(contentEt.getText().toString());
        });
        addDlg = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(v)
                .create();
        if (addDlg.getWindow() != null) {
            addDlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        passwdDlg = new DialogPopupWindow(
                this,
                new DialogInfo()
                        .setTitle(getString(R.string.set_access_password))
                        .setContent(getString(R.string.set_access_password_detail))
                        .setCancelText(getString(R.string.not_now))
                        .setConfirmText(getString(R.string.ok))
        );
        registerPopupWindow(passwdDlg);
    }

    public void confirmDialog(View view) {
        passwdDlg.dismiss();
        PinLockActivity.showSetPassed1(this);
    }

    public void cancelDialog(View view) {
        passwdDlg.dismiss();
    }

    private void initListView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new GalleryAdapter(R.layout.list_item_gallery, this);
        binding.list.setHasFixedSize(true);
        binding.list.setLayoutManager(layoutManager);
        binding.list.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }

    public void showSetting(View view) {
        MainSettingActivity.show(this);
    }

    public void switchMenu(View view) {
        L.d("fabOpen=%s", fabOpen);
        if (fabOpen) {
            closeMenu();
        } else {
            openMenu();
        }
    }

    public void showImgList(GalleryInfo info) {
        showImgList(info, ImgListMode.newLocalMode());
    }

    private void closeMenu() {
        fabOpen = false;
        ViewAnimator.animate(binding.fab).rotation(-135f, 0f)
                .andAnimate(binding.list).alpha(0.5f, 1f)
                .andAnimate(binding.importPhotoText, binding.addAlbumText).alpha(1f, 0f)
                .andAnimate(binding.importPhotoIcon, binding.addAlbumIcon).scale(1f, 0f)
                .duration(300L)
                .onStop(() -> {
                    setInvisibility(binding.importPhotoIcon,
                            binding.addAlbumIcon,
                            binding.importPhotoText,
                            binding.addAlbumText);
                })
                .start();
    }

    private void openMenu() {
        fabOpen = true;
        ViewAnimator.animate(binding.fab).rotation(0f, -135f)
                .andAnimate(binding.list).alpha(1f, 0.5f)
                .andAnimate(binding.importPhotoText, binding.addAlbumText).alpha(0f, 1f)
                .andAnimate(binding.importPhotoIcon, binding.addAlbumIcon).scale(0f, 1f)
                .duration(300L)
                .onStart(() -> {
                    setVisibility(binding.importPhotoIcon,
                            binding.addAlbumIcon,
                            binding.importPhotoText,
                            binding.addAlbumText);
                })
                .start();
    }

    public void importPhoto(View view) {
        closeMenu();
//        ImgListActivity.show(this, REQUEST_CODE_IMPORT_IMG);
    }

    public void showAddAlbum(View view) {
        closeMenu();
        contentEt.setText("");
        addDlg.show();
    }

    public String getCardSubtitle(Context ctx, GalleryInfo info) {
        if (info.getTotalPhoto() == 0) {
            return "";
        }
        return String.format(ctx.getString(R.string.gallery_subtitle_format1), String.valueOf(info.getTotalPhoto()));
    }

    @Override
    public void showGalleryList(List<GalleryInfo> list) {
        binding.srf.setRefreshing(false);
        L.d("size=" + list.size());
        adapter.replaceData(list);
    }

    @Override
    public void showAddGallerySuccess(GalleryInfo info) {
        addDlg.dismiss();
        hideSoftInput();
        showImgList(info, ImgListMode.newLocalMode());
    }

    private void showImgList(GalleryInfo info, ImgListMode mode) {
        SysImgListActivity.show(this, REQUEST_CODE_VIEW_IMG_LIST, info, mode);
    }

    @Override
    public void setPresenter(GalleryContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private static final class GalleryAdapter extends BaseRecyclerViewAdapter<GalleryInfo> {

        private int layoutId;
        private GalleryActivity activity;

        public GalleryAdapter(int layoutId, GalleryActivity activity) {
            this.layoutId = layoutId;
            this.activity = activity;
        }

        @Override
        protected int getLayoutId(int viewType) {
            return layoutId;
        }

        @Override
        protected void onBindViewHolder(BaseRecyclerViewHolder holder, int position, GalleryInfo info) {
            ListItemGalleryBinding binding = (ListItemGalleryBinding) holder.getBinding();
            binding.setEvent(activity);
//            binding.set
//            binding.itemContainer.setOnClickListener(v -> {
//                ImgListActivity.show();
//            });
        }
    }
}
