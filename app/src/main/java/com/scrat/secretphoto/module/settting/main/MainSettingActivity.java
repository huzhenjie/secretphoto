package com.scrat.secretphoto.module.settting.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.framework.common.BaseActivity;

public class MainSettingActivity extends BaseActivity {
    public static void show(Context context) {
        Intent i = new Intent(context, MainSettingActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_setting);
    }
}
