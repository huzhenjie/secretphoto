package com.scrat.secretphoto.module.img.detail;

import com.scrat.secretphoto.data.DataRepository;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;
import com.scrat.secretphoto.data.local.dao.img.ImgEntry;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.data.model.ImgInfo;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ImgPagerPresenter implements ImgPagerContract.Presenter {
    private ImgPagerContract.View view;
    private CompositeDisposable compositeDisposable;
    private GalleryInfo galleryInfo;

    public ImgPagerPresenter(ImgPagerContract.View view, GalleryInfo galleryInfo) {
        this.view = view;
        this.galleryInfo = galleryInfo;
        compositeDisposable = new CompositeDisposable();
        view.setPresenter(this);
    }

    @Override
    public void loadImgList() {
        Disposable disposable = DataRepository.getInstance().getImgDao()
                .getImgList(galleryInfo.getGalleryId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    view.showImg(list);
                });
        compositeDisposable.clear();
        compositeDisposable.add(disposable);
    }

    @Override
    public void deleteImg(ImgInfo info) {
        DataRepository.getInstance().getImgDao().deleteImg(GalleryEntry.TRASH_GALLERY_ID, info.getMd5());
        DataRepository.getInstance().getImgDao().updateImg(
                ImgEntry.Builder.newInstance().galleryId(GalleryEntry.TRASH_GALLERY_ID),
                ImgEntry.IMG_ID + "=?",
                String.valueOf(info.getImgId())
        );
        DataRepository.getInstance().getGalleryDao().decreaseImgCount(info.getGalleryId());
        DataRepository.getInstance().getGalleryDao().increaseImgCount(GalleryEntry.TRASH_GALLERY_ID);
        view.showDeleteImgSuccess();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }
}
