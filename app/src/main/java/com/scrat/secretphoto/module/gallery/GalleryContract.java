package com.scrat.secretphoto.module.gallery;

import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.framework.common.BasePresenter;
import com.scrat.secretphoto.framework.common.BaseView;

import java.util.List;

public interface GalleryContract {
    interface View extends BaseView<Presenter> {
        void showGalleryList(List<GalleryInfo> list);

        void showAddGallerySuccess(GalleryInfo info);
    }

    interface Presenter extends BasePresenter {
        void loadGalleryList();

        void addGallery(String title);
    }
}
