package com.scrat.secretphoto.module.img.detail;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.model.DialogInfo;
import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.databinding.FragmentImgDetailBinding;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.widget.DialogPopupWindow;

public class ImgDetailFragment extends BaseActivity implements ImgDetailContract.View {
    private static final String DATA = "data";
    private FragmentImgDetailBinding binding;
    private DialogPopupWindow dialog;
    private ImgInfo info;
    private ImgDetailContract.Presenter presenter;

    public static void show(Activity activity, int requestCode, ImgInfo info) {
        Intent i = new Intent(activity, ImgDetailFragment.class);
        i.putExtra(DATA, info);
        activity.startActivityForResult(i, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_img_detail);
        info = (ImgInfo) getIntent().getSerializableExtra(DATA);
        binding.setItem(info);

        initPresenter();

        initDialog();
    }

    private void initPresenter() {
        new ImgDetailPresenter(this, info);
    }

    private void initDialog() {
        dialog = new DialogPopupWindow(
                this,
                new DialogInfo()
                        .setTitle(getString(R.string.delete_photo))
                        .setContent(getString(R.string.delete_photo_content))
                        .setCancelText(getString(R.string.no))
                        .setCancelText(getString(R.string.yes))
        );
        registerPopupWindow(dialog);
    }

    public void deleteImg(View view) {
        dialog.showAsDropDown(binding.getRoot());
    }

    public void cancelDialog(View view) {
        dialog.dismiss();
    }

    public void confirmDialog(View view) {
        dialog.dismiss();
        presenter.deleteImg();
    }

    @Override
    public void showDeleteImgSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void setPresenter(ImgDetailContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
