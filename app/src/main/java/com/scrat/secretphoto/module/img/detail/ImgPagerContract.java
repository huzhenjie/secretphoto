package com.scrat.secretphoto.module.img.detail;

import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.framework.common.BasePresenter;
import com.scrat.secretphoto.framework.common.BaseView;

import java.util.List;

public interface ImgPagerContract {
    interface Presenter extends BasePresenter {
        void loadImgList();

        void deleteImg(ImgInfo imgInfo);
    }

    interface View extends BaseView<Presenter> {
        void showImg(List<ImgInfo> list);

        void showDeleteImgSuccess();
    }
}
