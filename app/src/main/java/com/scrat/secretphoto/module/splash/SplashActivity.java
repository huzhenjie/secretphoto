package com.scrat.secretphoto.module.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.module.gallery.GalleryActivity;
import com.scrat.secretphoto.module.guide.GuideActivity;
import com.scrat.secretphoto.module.lock.pin.PinLockActivity;

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView cover = findViewById(R.id.cover);
        cover.postDelayed(() -> {
            nextPage();
            finish();
        }, 1000L);
    }

    public void nextPage() {
        if (!Preferences.getInstance().hasShownGuide()) {
            GuideActivity.show(this);
            return;
        }

        if (Preferences.getInstance().hasSetPasswd()) {
            PinLockActivity.show(this);
            return;
        }

        GalleryActivity.show(this);
    }
}
