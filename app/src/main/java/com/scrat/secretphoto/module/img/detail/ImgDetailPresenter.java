package com.scrat.secretphoto.module.img.detail;

import com.scrat.secretphoto.data.DataRepository;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;
import com.scrat.secretphoto.data.local.dao.img.ImgEntry;
import com.scrat.secretphoto.data.model.ImgInfo;

public class ImgDetailPresenter implements ImgDetailContract.Presenter {

    private ImgDetailContract.View view;
    private ImgInfo info;

    public ImgDetailPresenter(ImgDetailContract.View view, ImgInfo info) {
        this.info = info;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void deleteImg() {
        DataRepository.getInstance().getImgDao().deleteImg(GalleryEntry.TRASH_GALLERY_ID, info.getMd5());
        DataRepository.getInstance().getImgDao().updateImg(
                ImgEntry.Builder.newInstance().galleryId(GalleryEntry.TRASH_GALLERY_ID),
                ImgEntry.IMG_ID + "=?",
                String.valueOf(info.getImgId())
        );
        DataRepository.getInstance().getGalleryDao().decreaseImgCount(info.getGalleryId());
        DataRepository.getInstance().getGalleryDao().increaseImgCount(GalleryEntry.TRASH_GALLERY_ID);
        view.showDeleteImgSuccess();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }
}
