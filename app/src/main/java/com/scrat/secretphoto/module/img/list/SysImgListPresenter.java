package com.scrat.secretphoto.module.img.list;

import android.text.TextUtils;

import com.scrat.secretphoto.data.DataRepository;
import com.scrat.secretphoto.data.FileManager;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;
import com.scrat.secretphoto.data.local.dao.img.ImgEntry;
import com.scrat.secretphoto.data.local.media.MediaStoreHelper;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.data.model.ImgListMode;
import com.scrat.secretphoto.framework.util.CollectionUtil;
import com.scrat.secretphoto.framework.util.L;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SysImgListPresenter implements SysImgListContract.Presenter {
    private SysImgListContract.View view;
    private CompositeDisposable compositeDisposable;
    private GalleryInfo galleryInfo;
    private ImgListMode mode;

    public SysImgListPresenter(SysImgListContract.View view, GalleryInfo info, ImgListMode mode) {
        this.mode = mode;
        galleryInfo = info;
        this.view = view;
        compositeDisposable = new CompositeDisposable();
        this.view.setPresenter(this);
    }

    @Override
    public void loadImgList() {
        Disposable disposable;
        if (mode.isImportImg()) {
            Observable<List<ImgInfo>> observable
                    = MediaStoreHelper.getImgList(view.getApplicationContext());
            Consumer<List<ImgInfo>> consumer = list -> {
                view.showImg(list);
            };
            disposable = observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(consumer);
        } else {
            disposable = DataRepository.getInstance().getImgDao()
                    .getImgList(galleryInfo.getGalleryId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(list -> {
                        view.showImg(list);
                    });
        }
        compositeDisposable.clear();
        compositeDisposable.add(disposable);
    }

    @Override
    public void importImgList(List<ImgInfo> list) {
        if (CollectionUtil.isEmpty(list)) {
            return;
        }

        view.importBegin(list.size());

        Consumer<ImgInfo> consumer = info -> {
            view.importSuccess(info);
        };

        Observable<ImgInfo> observable = Observable.create(emitter -> {
            File folder = FileManager.getAbsSecretImgFolder();
            for (ImgInfo info : list) {
                File file = new File(info.getOriginPath());
                String md5 = FileManager.md5(file);
                if (TextUtils.isEmpty(md5)) {
                    L.e("md5 is empty:%s", info.getOriginPath());
                    continue;
                }
                File targetFile = new File(folder, md5);
                boolean copySuccess = FileManager.copy(file, targetFile);
                if (!copySuccess) {
                    L.e("copy fail. origin:%s target:%s", info.getOriginPath(), targetFile.getAbsolutePath());
                    continue;
                }
                L.d("copy success. origin:%s target:%s", info.getOriginPath(), targetFile.getAbsolutePath());
                long imgId = DataRepository.getInstance().getImgDao().addImg(
                        ImgEntry.Builder.newInstance()
                                .createdAt(System.currentTimeMillis())
                                .fileSize(file.length())
                                .fileName(file.getName())
                                .md5(md5)
                                .originPath(info.getOriginPath())
                                .galleryId(galleryInfo.getGalleryId())
                                .mediaId(info.getMediaId())
                );
                L.d("img insert %s", imgId);
                int totalImg = DataRepository.getInstance().getImgDao().getTotalCount(galleryInfo.getGalleryId());
                String cover = DataRepository.getInstance().getImgDao().getCover(galleryInfo.getGalleryId());
                DataRepository.getInstance().getGalleryDao().updateGallery(
                        GalleryEntry.Builder.newInstance()
                                .totalPhoto(totalImg)
                                .cover(cover),
                        GalleryEntry.GALLERY_ID + "=?",
                        String.valueOf(galleryInfo.getGalleryId()));
                DataRepository.getInstance().getGalleryDao().increaseImgCount(galleryInfo.getGalleryId());

                emitter.onNext(info);
            }
            emitter.onComplete();
        });

        compositeDisposable.clear();
        Disposable disposable = observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> view.importComplete())
                .subscribe(consumer);
        compositeDisposable.add(disposable);
    }

    @Override
    public void removeImgList(List<ImgInfo> list) {
        int total = list.size();
        view.removeBegin(total);
        Consumer<List<ImgInfo>> consumer = fails -> {
            int totalFail = fails.size();
            view.removeComplete(total - totalFail, totalFail);
        };
        compositeDisposable.clear();
        Disposable disposable = MediaStoreHelper.removeImgList(view.getApplicationContext(), list)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer);
        compositeDisposable.add(disposable);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }
}
