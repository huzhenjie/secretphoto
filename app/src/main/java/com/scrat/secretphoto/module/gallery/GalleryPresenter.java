package com.scrat.secretphoto.module.gallery;

import android.text.TextUtils;

import com.scrat.secretphoto.data.DataRepository;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.local.dao.gallery.GalleryEntry;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.framework.util.L;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class GalleryPresenter implements GalleryContract.Presenter {
    private GalleryContract.View view;
    private CompositeDisposable compositeDisposable;

    public GalleryPresenter(GalleryContract.View view) {
        this.view = view;
        compositeDisposable = new CompositeDisposable();
        this.view.setPresenter(this);
    }

    @Override
    public void loadGalleryList() {
        compositeDisposable.clear();
        Disposable disposable = DataRepository.getInstance().getGalleryDao()
                .getGalleryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(galleryList -> {
                    galleryList.add(0, GalleryInfo.getMainAlbum(view.getApplicationContext()));
                    if (Preferences.getInstance().getTotalTrashImgCount() > 0) {
                        galleryList.add(GalleryInfo.getTrash(view.getApplicationContext()));
                    }
                    view.showGalleryList(galleryList);
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void addGallery(String title) {
        if (TextUtils.isEmpty(title)) {
            return;
        }
        long galleryId = DataRepository.getInstance().getGalleryDao()
                .addGallery(new GalleryEntry.Builder().galleryName(title));
        L.d("galleryId=%s", galleryId);
        compositeDisposable.clear();
        Disposable disposable = DataRepository.getInstance().getGalleryDao().getGallery(galleryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(gallery -> {
                    view.showAddGallerySuccess(gallery);
                    loadGalleryList();
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }
}
