package com.scrat.secretphoto.module.img.list;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.scrat.secretphoto.BR;
import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.model.DialogInfo;
import com.scrat.secretphoto.data.model.GalleryInfo;
import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.data.model.ImgListMode;
import com.scrat.secretphoto.databinding.ActivityImgListBinding;
import com.scrat.secretphoto.databinding.ListItemImgBinding;
import com.scrat.secretphoto.framework.common.BaseActivity;
import com.scrat.secretphoto.framework.common.BaseRecyclerViewAdapter;
import com.scrat.secretphoto.framework.common.BaseRecyclerViewHolder;
import com.scrat.secretphoto.framework.util.L;
import com.scrat.secretphoto.framework.util.PermissionUtil;
import com.scrat.secretphoto.framework.viewanimator.ViewAnimator;
import com.scrat.secretphoto.module.img.detail.ImgDetailFragment;
import com.scrat.secretphoto.module.img.detail.ImgPagerActivity;
import com.scrat.secretphoto.module.lock.pin.PinLockActivity;
import com.scrat.secretphoto.module.settting.gallery.GallerySettingActivity;
import com.scrat.secretphoto.widget.DialogPopupWindow;
import com.scrat.secretphoto.widget.GridSpacingItemDecoration;
import com.scrat.secretphoto.widget.ProgressPopupWindow;

import java.util.ArrayList;
import java.util.List;

public class SysImgListActivity extends BaseActivity implements SysImgListContract.View {
    private static final int MAX_COL_OF_ROW = 4;
    private static final int REQUEST_CODE_IMPORT_IMG = 1;
    private static final int REQUEST_CODE_VIEW_IMG = 2;
    private static final int REQUEST_CODE_SETTING = 3;
    private static final String DATA = "data";
    private static final String MODE = "mode";
    private ActivityImgListBinding binding;
    private SysImgListContract.Presenter presenter;
    private ImgListAdapter adapter;
    private ProgressPopupWindow popupWindow;
    private DialogPopupWindow passwdDlg;
    private ImgListMode mode;
    private GalleryInfo info;
    private boolean checkedAll = false;

    public static void show(Activity activity, int requestCode, GalleryInfo info, ImgListMode mode) {
        Intent i = new Intent(activity, SysImgListActivity.class);
        i.putExtra(DATA, info);
        i.putExtra(MODE, mode);
        activity.startActivityForResult(i, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_img_list);
        info = (GalleryInfo) getIntent().getSerializableExtra(DATA);
        mode = (ImgListMode) getIntent().getSerializableExtra(MODE);
        mode.setGalleryName(info.getTitle()).setGalleryId(info.getGalleryId());
        binding.setMode(mode);
        binding.setEvent(this);
        initSwipeRefreshLayout(binding.srl);
        initListView();
        initPresenter(mode);
        presenter.loadImgList();
        initPopupWindow();
        PermissionUtil.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_IMPORT_IMG:
                presenter.loadImgList();
                binding.tip.setVisibility(View.VISIBLE);
                ViewAnimator.animate(binding.tip)
                        .translationX(-100f, 0f)
                        .duration(500)
                        .onStop(() -> {
                            binding.list.postDelayed(
                                    () -> binding.tip.setVisibility(View.GONE),
                                    1500L
                            );
                        })
                        .start();
                passwdCheck();
                break;
            case REQUEST_CODE_VIEW_IMG:
                presenter.loadImgList();
                showMessage(R.string.photo_deleted);
                break;
            case REQUEST_CODE_SETTING:
                info = (GalleryInfo) data.getSerializableExtra(DATA);
                break;
        }
    }

    private void initPresenter(ImgListMode mode) {
        new SysImgListPresenter(this, info, mode);
    }

    private void initPopupWindow() {
        popupWindow = new ProgressPopupWindow(binding.getRoot().getContext());
        registerPopupWindow(popupWindow);
        passwdDlg = new DialogPopupWindow(
                this,
                new DialogInfo()
                        .setTitle(getString(R.string.set_access_password))
                        .setContent(getString(R.string.set_access_password_detail))
                        .setCancelText(getString(R.string.not_now))
                        .setConfirmText(getString(R.string.ok))
        );
        registerPopupWindow(passwdDlg);
    }

    private void passwdCheck() {
        if (!Preferences.getInstance().hasSetPasswd()) {
            passwdDlg.showAsDropDown(binding.toolBarTitle);
        }
    }

    public void confirmDialog(View view) {
        passwdDlg.dismiss();
        PinLockActivity.showSetPassed1(this);
    }

    public void cancelDialog(View view) {
        passwdDlg.dismiss();
    }

    private void initListView() {
        binding.list.setHasFixedSize(true);

        GridLayoutManager layoutManager = new GridLayoutManager(this, MAX_COL_OF_ROW);
        binding.list.setLayoutManager(layoutManager);
        GridSpacingItemDecoration decoration = new GridSpacingItemDecoration(
                MAX_COL_OF_ROW, 1, false);
        binding.list.addItemDecoration(decoration);
        adapter = new ImgListAdapter(mode, this, info, (total, totalChecked) -> {
            mode.setTotal(total);
            mode.setTotalChecked(totalChecked);
            binding.setMode(mode);
            binding.executePendingBindings();
        });
        binding.list.setAdapter(adapter);
    }

    @Override
    protected void initSwipeRefreshLayout(SwipeRefreshLayout srl) {
        super.initSwipeRefreshLayout(srl);
        srl.setOnRefreshListener(() -> presenter.loadImgList());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }

    public String getTitle(Context ctx, ImgListMode mode) {
        if (mode.isLocalList()) {
            return mode.getGalleryName();
        }
        if (mode.getTotalChecked() == 0) {
            return ctx.getString(R.string.import_photo);
        }
        if (mode.getTotalChecked() == mode.getTotal()) {
            return ctx.getString(R.string.all_photo_selected);
        }
        return String.format(ctx.getString(R.string.selected_format),
                String.valueOf(mode.getTotalChecked()), String.valueOf(mode.getTotal()));
    }

    public void checked(View view) {
        if (checkedAll) {
            binding.checked.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox));
            adapter.uncheckedAll();
        } else {
            binding.checked.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_delete_un));
            adapter.checkedAll();
        }
        checkedAll = !checkedAll;
    }

    public void setting(View view) {
        GallerySettingActivity.show(this, REQUEST_CODE_SETTING, info);
    }

    public void importPhoto(View view) {
        presenter.importImgList(adapter.getChecked());
    }

    public void showImportPhoto(View view) {
        SysImgListActivity.show(this, REQUEST_CODE_IMPORT_IMG, info, ImgListMode.newSysMode());
    }

    @Override
    public void showImg(List<ImgInfo> list) {
        binding.srl.setRefreshing(false);
        adapter.replaceData(list);
        mode.setTotal(list.size());
        mode.setTotalChecked(0);
        binding.setMode(mode);
        binding.executePendingBindings();
    }

    @Override
    public void importBegin(int total) {
        L.d("import begin %s", total);
        popupWindow.showAsDropDown(binding.toolBarTitle);
    }

    @Override
    public void importSuccess(ImgInfo info) {
        L.d("import success:%s", info.toString());
    }

    @Override
    public void importComplete() {
        L.d("import complete");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void removeBegin(int total) {
        L.d("remove begin %s", total);
    }

    @Override
    public void removeComplete(int totalSuccess, int totalFail) {
        L.d("remove complete");
    }

    @Override
    public void setPresenter(SysImgListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private static interface CheckedModifyListener {
        void notifyCheckedCountChange(int total, int totalChecked);
    }

    private static class ImgListAdapter extends BaseRecyclerViewAdapter<ImgInfo> {
        private ImgListMode mode;
        private Activity activity;
        private CheckedModifyListener listener;
        private GalleryInfo galleryInfo;

        public ImgListAdapter(
                ImgListMode mode,
                Activity activity,
                GalleryInfo galleryInfo,
                CheckedModifyListener listener) {
            this.listener = listener;
            this.galleryInfo = galleryInfo;
            this.mode = mode;
            this.activity = activity;
        }

        public void checkedAll() {
            for (ImgInfo info : list) {
                info.setChecked(true);
            }
            notifyDataSetChanged();
            listener.notifyCheckedCountChange(getItemCount(), getTotalCheck());
        }

        public void uncheckedAll() {
            for (ImgInfo info : list) {
                info.setChecked(false);
            }
            notifyDataSetChanged();
            listener.notifyCheckedCountChange(getItemCount(), getTotalCheck());
        }

        private int getTotalCheck() {
            int totalChecked = 0;
            for (ImgInfo info : list) {
                if (info.isChecked()) {
                    totalChecked++;
                }
            }
            return totalChecked;
        }

        public List<ImgInfo> getChecked() {
            List<ImgInfo> checkedList = new ArrayList<>();
            for (ImgInfo info : list) {
                if (info.isChecked()) {
                    checkedList.add(info);
                }
            }
            return checkedList;
        }

        @Override
        protected int getLayoutId(int viewType) {
            return R.layout.list_item_img;
        }

        @Override
        public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position, ImgInfo info) {
            ListItemImgBinding binding = (ListItemImgBinding) holder.getBinding();
            binding.imgContainer.setOnClickListener(v -> {
                if (mode.isImportImg()) {
                    info.setChecked(!info.isChecked());
                    binding.setVariable(BR.item, info);
                    binding.executePendingBindings();
                    listener.notifyCheckedCountChange(getItemCount(), getTotalCheck());
                } else {
                    ImgPagerActivity.show(activity, REQUEST_CODE_VIEW_IMG, galleryInfo, position);
                }
            });
        }
    }
}
