package com.scrat.secretphoto.module.img.list;

import com.scrat.secretphoto.data.model.ImgInfo;
import com.scrat.secretphoto.framework.common.BasePresenter;
import com.scrat.secretphoto.framework.common.BaseView;

import java.util.List;

public interface SysImgListContract {
    interface View extends BaseView<Presenter> {

        void showImg(List<ImgInfo> list);

        void importBegin(int total);

        void importSuccess(ImgInfo info);

        void importComplete();

        void removeBegin(int total);

        void removeComplete(int totalSuccess, int totalFail);
    }

    interface Presenter extends BasePresenter {
        void loadImgList();

        void importImgList(List<ImgInfo> list);

        void removeImgList(List<ImgInfo> list);

    }
}
