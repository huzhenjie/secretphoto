package com.scrat.secretphoto.module.lock.pin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.scrat.secretphoto.R;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.model.PinCodeViewModel;
import com.scrat.secretphoto.databinding.ActivityPinCodeBinding;
import com.scrat.secretphoto.framework.viewanimator.ViewAnimator;
import com.scrat.secretphoto.module.gallery.GalleryActivity;

public class PinLockActivity extends AppCompatActivity {
    private static final String STATE = "state";
    private static final String DATA = "data";
    private static final int STATE_DEFAULT = 0;
    private static final int STATE_SET_PASSWD1 = 1;
    private static final int STATE_SET_PASSWD2 = 2;

    public static void show(Context ctx) {
        Intent i = new Intent(ctx, PinLockActivity.class);
        ctx.startActivity(i);
    }

    public static void showSetPassed1(Context ctx) {
        Intent i = new Intent(ctx, PinLockActivity.class);
        i.putExtra(STATE, STATE_SET_PASSWD1);
        ctx.startActivity(i);
    }

    public static void showSetPassed2(Activity ctx, int requestCode) {
        Intent i = new Intent(ctx, PinLockActivity.class);
        i.putExtra(STATE, STATE_SET_PASSWD2);
        ctx.startActivityForResult(i, requestCode);
    }

    private PinCodeViewModel model;
    private ActivityPinCodeBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pin_code);
        int state = getIntent().getIntExtra(STATE, STATE_DEFAULT);
        model = new PinCodeViewModel(code -> {
            switch (state) {
                case STATE_DEFAULT:
                    if (Preferences.getInstance().getPinCodePasswd().equals(model.getSelectedCode())) {
                        GalleryActivity.show(this);
                        finish();
                    } else {
                        showPinCodeWrong();
                    }
                    break;
                case STATE_SET_PASSWD1:
                    PinLockActivity.showSetPassed2(this, STATE_SET_PASSWD2);
                    break;
                case STATE_SET_PASSWD2:
                    Intent i = new Intent();
                    i.putExtra(DATA, code);
                    setResult(RESULT_OK, i);
                    finish();
                    break;
            }
        });
        switch (state) {
            case STATE_SET_PASSWD1:
                model.setPasswordStep1();
                break;
            case STATE_SET_PASSWD2:
                model.setPasswordStep2();
                break;
        }

        binding.setItem(model);
    }

    public void showPinCodeWrong() {
        ViewAnimator.animate(binding.passwdPanel)
                .duration(1000L)
                .wave()
                .start();
        model.clear();
        binding.setItem(model);
        binding.executePendingBindings();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case STATE_SET_PASSWD2:
                String passwd = data.getStringExtra(DATA);
                if (!TextUtils.isEmpty(passwd) && passwd.equals(model.getSelectedCode())) {
                    Preferences.getInstance().setPinCodePasswd(passwd);
                    GalleryActivity.show(this);
                    finish();
                    return;
                }
                showPinCodeWrong();
                break;
        }
    }

    public void clear(View view) {
        model.clear();
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item0(View view) {
        model.appendSelectedCode("0");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item1(View view) {
        model.appendSelectedCode("1");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item2(View view) {
        model.appendSelectedCode("2");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item3(View view) {
        model.appendSelectedCode("3");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item4(View view) {
        model.appendSelectedCode("4");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item5(View view) {
        model.appendSelectedCode("5");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item6(View view) {
        model.appendSelectedCode("6");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item7(View view) {
        model.appendSelectedCode("7");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item8(View view) {
        model.appendSelectedCode("8");
        binding.setItem(model);
        binding.executePendingBindings();
    }

    public void item9(View view) {
        model.appendSelectedCode("9");
        binding.setItem(model);
        binding.executePendingBindings();
    }

}
