package com.scrat.secretphoto.widget;

import android.content.Context;
import android.view.LayoutInflater;

import com.scrat.secretphoto.data.model.DialogInfo;
import com.scrat.secretphoto.databinding.PopupWindowDialogBinding;
import com.scrat.secretphoto.framework.common.BasePopupWindow;

public class DialogPopupWindow extends BasePopupWindow {
    private PopupWindowDialogBinding binding;

    public DialogPopupWindow(Context context, DialogInfo info) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = PopupWindowDialogBinding.inflate(inflater);
        binding.panel.setOnClickListener(v -> {
        });
        initPopupWindow(binding.getRoot());
        binding.setItem(info);
    }

}
