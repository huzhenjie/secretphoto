package com.scrat.secretphoto.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.scrat.secretphoto.databinding.PopupWindowProcessBinding;

public class ProgressPopupWindow extends PopupWindow {
    private PopupWindowProcessBinding binding;

    public ProgressPopupWindow(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = PopupWindowProcessBinding.inflate(inflater);
        setContentView(binding.getRoot());
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        setBackgroundDrawable(dw);
    }
}
