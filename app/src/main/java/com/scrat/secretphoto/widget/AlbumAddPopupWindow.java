package com.scrat.secretphoto.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.scrat.secretphoto.databinding.PopupWindowAddAlbumBinding;

public class AlbumAddPopupWindow extends PopupWindow {
    private PopupWindowAddAlbumBinding binding;

    public AlbumAddPopupWindow(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = PopupWindowAddAlbumBinding.inflate(inflater);
        setContentView(binding.getRoot());
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        setBackgroundDrawable(dw);

        setOutsideTouchable(true);
        setFocusable(true);
    }

    public String getContent() {
        return binding.albumName.getText().toString();
    }

    public void show(View view) {
        binding.albumName.setText("");
        showAsDropDown(view);
    }
}
