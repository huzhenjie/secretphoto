package com.scrat.secretphoto;

import android.app.Application;
import android.content.Context;

import com.scrat.secretphoto.data.local.dao.lib.DbHelper;
import com.scrat.secretphoto.data.local.Preferences;
import com.scrat.secretphoto.data.local.env.EnvChecker;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Context ctx = getApplicationContext();
        EnvChecker.refresh(ctx);
        Preferences.getInstance().init(ctx);
        DbHelper.getInstance().init(ctx);
//        DbHelper.getInstance().getDb()
//                .createQuery(GalleryInfo.TABLE, "select * from " + GalleryInfo.TABLE)
//                .mapToList(GalleryInfo.MAPPER)
//                .toFlowable()

//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(galleryInfos -> {
//                    L.e("xxxx size=", galleryInfos.size());
//                });

    }
}
